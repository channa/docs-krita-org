# Translation of docs_krita_org_tutorials___krita-brush-tips___bokeh-brush.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-31 16:45+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_01.png\n"
"   :alt: krita bokeh brush setup background"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_01.png\n"
"   :alt: Ajustament del fons del pinzell amb efecte Bokeh en el Krita."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_02.png\n"
"   :alt: Krita bokeh brush tips scatter settings"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_02.png\n"
"   :alt: Ajustaments per a la dispersió de les puntes de pinzell amb efecte "
"Bokeh en el Krita."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_03.png\n"
"   :alt: Choosing the brush tip for the bokeh effect"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_03.png\n"
"   :alt: Triant la punta del pinzell per a l'efecte Bokeh."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_04.png\n"
"   :alt: paint the bokeh circles on the background"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-bokeh_04.png\n"
"   :alt: Pintant els cercles amb l'efecte Bokeh en el fons."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:1
msgid "Creating bokeh effect with the help of some simple brush tip."
msgstr "Crear l'efecte Bokeh amb l'ajuda d'alguna punta de pinzell senzilla."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:13
msgid "Brush Tips: Bokeh"
msgstr "Puntes de pinzell: efecte Bokeh"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:16
msgid "Question"
msgstr "Pregunta"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:18
msgid "How do you do bokeh effects?"
msgstr "Com es fan els efectes Bokeh?"

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:20
msgid "First, blur your image with the Lens Blur to roughly 50 pixels."
msgstr ""
"Primer, desenfoqueu la vostra imatge amb el Difuminat de la lent a "
"aproximadament 50 píxels."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:26
msgid "Take smudge_textured, add scattering, turn off tablet input."
msgstr ""
"Preneu «Smudge_textured», afegiu Dispersió i apagueu l'entrada de la tauleta."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:31
msgid ""
"Change the brush-tip to ‘Bokeh’ and check ‘overlay’ (you will want to play "
"with the spacing as well)."
msgstr ""
"Canvieu la punta del pinzell a «Efecte Bokeh» i marqueu "
"«Superposició» (també voldreu jugar amb l'Espaiat)."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:36
msgid ""
"Then make a new layer over your drawing, set that to ‘lighter color’ (it’s "
"under lighter category) and painter over it with you brush."
msgstr ""
"Després, creeu una capa nova sobre el vostre dibuix, establiu-lo al «Color "
"més clar» (es troba a la categoria més clara) i pinteu sobre seu amb el "
"vostre pinzell."

#: ../../tutorials/krita-brush-tips/bokeh-brush.rst:41
msgid ""
"Overlay mode on the smudge brush allows you to sample all the layers, and "
"the ‘lighter color’ blending mode makes sure that the Bokeh circles only "
"show up when they are a lighter color than the original pixels underneath. "
"You can further modify this brush by adding a ‘fuzzy’ sensor to the spacing "
"and size options, changing the brush blending mode to ‘addition’, or by "
"choosing a different brush-tip."
msgstr ""
"El mode Superposició sobre el pinzell amb esborronat permet mostrejar totes "
"les capes, i el mode de barreja «Color més clar» assegura que els cercles "
"amb efecte Bokeh només es mostrin quan tinguin un color més clar que els "
"píxels originals que hi hagi a sota. Podreu modificar encara més aquest "
"pinzell afegint un sensor «difús» a les opcions per a l'Espaiat i Mida, "
"canviant el mode de barreja del pinzell a «Addició» o triant una punta de "
"pinzell diferent."
