# Translation of docs_krita_org_reference_manual___preferences___general_settings.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
# Josep Ma. Ferrer <txemaq@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-14 03:19+0200\n"
"PO-Revision-Date: 2019-08-17 18:19+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../<generated>:1
msgid "Recalculate animation cache in background."
msgstr "Torna a calcular la memòria cau de l'animació en segon pla."

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Krita_Preferences_General.png"
msgstr ".. image:: images/preferences/Krita_Preferences_General.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_tool_icon.png"
msgstr ".. image:: images/preferences/Settings_cursor_tool_icon.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_arrow.png"
msgstr ".. image:: images/preferences/Settings_cursor_arrow.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_crosshair.png"
msgstr ".. image:: images/preferences/Settings_cursor_crosshair.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_small_circle.png"
msgstr ".. image:: images/preferences/Settings_cursor_small_circle.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_no_cursor.png"
msgstr ".. image:: images/preferences/Settings_cursor_no_cursor.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_triangle_righthanded.png"
msgstr ".. image:: images/preferences/Settings_cursor_triangle_righthanded.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_triangle_lefthanded.png"
msgstr ".. image:: images/preferences/Settings_cursor_triangle_lefthanded.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_black_pixel.png"
msgstr ".. image:: images/preferences/Settings_cursor_black_pixel.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Settings_cursor_white_pixel.png"
msgstr ".. image:: images/preferences/Settings_cursor_white_pixel.png"

#: ../../reference_manual/preferences/general_settings.rst:0
msgid ".. image:: images/preferences/Krita_4_0_kinetic_scrolling.gif"
msgstr ".. image:: images/preferences/Krita_4_0_kinetic_scrolling.gif"

#: ../../reference_manual/preferences/general_settings.rst:1
msgid "General Preferences in Krita."
msgstr "Preferències generals al Krita."

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Preferences"
msgstr "Preferències"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Settings"
msgstr "Ajustaments"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Cursor"
msgstr "Cursor"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Autosave"
msgstr "Desa automàticament"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Tabbed Documents"
msgstr "Documents en pestanyes"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Subwindow Documents"
msgstr "Documents en subfinestres"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Pop up palette"
msgstr "Paleta emergent"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "File Dialog"
msgstr "Diàleg de fitxer"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Maximum Brush Size"
msgstr "Mida màxima del pinzell"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Kinetic Scrolling"
msgstr "Desplaçament cinètic"

#: ../../reference_manual/preferences/general_settings.rst:12
msgid "Sessions"
msgstr "Sessions"

#: ../../reference_manual/preferences/general_settings.rst:17
msgid "General Settings"
msgstr "Ajustaments generals"

#: ../../reference_manual/preferences/general_settings.rst:19
msgid ""
"You can access the General Category of the preferences by first going to  :"
"menuselection:`Settings --> Configure Krita`."
msgstr ""
"Podeu accedir a la categoria General de les preferències anant primer a :"
"menuselection:`Arranjament --> Configura el Krita`."

#: ../../reference_manual/preferences/general_settings.rst:24
msgid "Cursor Settings"
msgstr "Ajustaments per al cursor"

#: ../../reference_manual/preferences/general_settings.rst:26
msgid "Customize the drawing cursor here:"
msgstr "Personalitzeu aquí el cursor de dibuix:"

#: ../../reference_manual/preferences/general_settings.rst:29
msgid "Cursor Shape"
msgstr "Forma del cursor"

#: ../../reference_manual/preferences/general_settings.rst:31
msgid ""
"Select a cursor shape to use while the brush tools are used. This cursor "
"will always be visible on the canvas. It is usually set to a type exactly "
"where your pen nib is at. The available cursor types are shown below."
msgstr ""
"Seleccioneu una forma de cursor per emprar mentre s'utilitzen les eines de "
"pinzell. Aquest cursor sempre estarà visible en el llenç. En general, "
"s'establirà a un tipus exactament on es troba la punta del llapis. Els tipus "
"de cursor disponibles es mostren a continuació."

#: ../../reference_manual/preferences/general_settings.rst:34
msgid "Shows the currently selected tool icon, even for the freehand brush."
msgstr ""
"Mostrarà la icona de l'eina seleccionada, fins i tot per al pinzell a mà "
"alçada."

#: ../../reference_manual/preferences/general_settings.rst:36
msgid "Tool Icon"
msgstr "Icona de l'eina"

#: ../../reference_manual/preferences/general_settings.rst:39
msgid "Shows a generic cursor."
msgstr "Mostrarà un cursor genèric."

#: ../../reference_manual/preferences/general_settings.rst:41
msgid "Arrow"
msgstr "Fletxa"

#: ../../reference_manual/preferences/general_settings.rst:44
msgid "Shows a precision reticule."
msgstr "Mostrarà una retícula de precisió."

#: ../../reference_manual/preferences/general_settings.rst:46
msgid "Crosshair"
msgstr "Creu"

#: ../../reference_manual/preferences/general_settings.rst:48
msgid "Small circle"
msgstr "Cercle petit"

#: ../../reference_manual/preferences/general_settings.rst:50
msgid "Shows a small white dot with a black outline."
msgstr "Mostrarà un petit punt blanc amb un contorn negre."

#: ../../reference_manual/preferences/general_settings.rst:55
msgid "Show no cursor, useful for tablet-monitors."
msgstr "No mostrarà cap cursor, útil per als monitors tàctils i tauletes."

#: ../../reference_manual/preferences/general_settings.rst:57
msgid "No Cursor"
msgstr "Sense cursor"

# skip-rule: punctuation-period
#: ../../reference_manual/preferences/general_settings.rst:59
msgid "Triangle Right-Handed."
msgstr "Triangle cap a la dreta"

#: ../../reference_manual/preferences/general_settings.rst:61
msgid "Gives a small white triangle with a black border."
msgstr "Donarà un petit triangle blanc amb una vora negra."

#: ../../reference_manual/preferences/general_settings.rst:66
msgid "Same as above but mirrored."
msgstr "Igual que l'anterior però reflectit."

# skip-rule: punctuation-period
#: ../../reference_manual/preferences/general_settings.rst:68
msgid "Triangle Left-Handed."
msgstr "Triangle cap a l'esquerra"

#: ../../reference_manual/preferences/general_settings.rst:71
msgid "Gives a single black pixel."
msgstr "Donarà un únic píxel negre."

#: ../../reference_manual/preferences/general_settings.rst:73
msgid "Black Pixel"
msgstr "Píxel negre"

#: ../../reference_manual/preferences/general_settings.rst:76
msgid "Gives a single white pixel."
msgstr "Donarà un únic píxel blanc."

#: ../../reference_manual/preferences/general_settings.rst:79
msgid "White Pixel"
msgstr "Píxel blanc"

#: ../../reference_manual/preferences/general_settings.rst:82
msgid "Outline Shape"
msgstr "Forma del contorn"

#: ../../reference_manual/preferences/general_settings.rst:84
msgid ""
"Select an outline shape to use while the brush tools are used. This cursor "
"shape will optionally show in the middle of a painting stroke as well. The "
"available outline shape types are shown below. (pictures will come soon)"
msgstr ""
"Seleccioneu una forma del contorn per emprar mentre s'utilitzen les eines de "
"pinzell. Aquesta forma del cursor també es mostrarà de manera opcional enmig "
"d'un traç de pintura. Els tipus disponibles de formes del contorn es mostren "
"a continuació (les imatges arribaran aviat)."

#: ../../reference_manual/preferences/general_settings.rst:86
msgid "No Outline"
msgstr "Sense contorn"

#: ../../reference_manual/preferences/general_settings.rst:87
msgid "No outline."
msgstr "Sense contorn."

#: ../../reference_manual/preferences/general_settings.rst:88
msgid "Circle Outline"
msgstr "Contorn de cercle"

#: ../../reference_manual/preferences/general_settings.rst:89
msgid "Gives a circular outline approximating the brush size."
msgstr "Donarà un contorn circular que s'aproximarà a la mida del pinzell."

#: ../../reference_manual/preferences/general_settings.rst:90
msgid "Preview Outline"
msgstr "Vista prèvia del contorn"

#: ../../reference_manual/preferences/general_settings.rst:91
msgid "Gives an outline based on the actual shape of the brush."
msgstr "Donarà un contorn basat en la forma real del pinzell."

#: ../../reference_manual/preferences/general_settings.rst:93
msgid "Gives a circular outline with a tilt-indicator."
msgstr "Donarà un contorn circular amb un indicador d'inclinació."

#: ../../reference_manual/preferences/general_settings.rst:95
msgid "Tilt Outline"
msgstr "Contorn de la inclinació"

#: ../../reference_manual/preferences/general_settings.rst:98
msgid "While Painting..."
msgstr "Mentre es pinta..."

#: ../../reference_manual/preferences/general_settings.rst:101
msgid ""
"This option when selected will show the brush outline while a stroke is "
"being made. If unchecked the brush outline will not appear during stroke "
"making, it will show up only after the brush stroke is finished. This option "
"works only when Brush Outline is selected as the Cursor Shape."
msgstr ""
"Quan se selecciona aquesta opció, es mostrarà el contorn del pinzell mentre "
"es realitza un traç. Si no se selecciona, el contorn del pinzell no "
"apareixerà durant l'execució del traç, es mostrarà només una vegada es "
"finalitzi la pinzellada. Aquesta opció només funcionarà quan se selecciona "
"Contorn del pinzell com a la Forma del cursor."

#: ../../reference_manual/preferences/general_settings.rst:105
msgid "Show Outline"
msgstr "Mostra el contorn"

#: ../../reference_manual/preferences/general_settings.rst:105
msgid "Used to be called \"Show Outline When Painting\"."
msgstr "Se solia anomenar «Mostra el contorn mentre es pinta»."

#: ../../reference_manual/preferences/general_settings.rst:107
msgid "Use effective outline size"
msgstr "Usa la mida de contorn efectiva"

#: ../../reference_manual/preferences/general_settings.rst:111
msgid ""
"This makes sure that the outline size will always be the maximum possible "
"brush diameter, and not the current one as affected by sensors such as "
"pressure. This makes the cursor a little less noisy to use."
msgstr ""
"Assegurarà que la mida del contorn sempre serà el màxim diàmetre possible "
"del pinzell, i no l'actual, afectat pels sensors com la pressió. Això farà "
"que el cursor sigui una mica menys sorollós emprar-lo."

#: ../../reference_manual/preferences/general_settings.rst:114
msgid ""
"The default cursor color. This is mixed with the canvas image so that it "
"will usually have a contrasting color, but sometimes this mixing does not "
"work. This is usually due driver problems. When that happens, you can "
"configure a more pleasant color here."
msgstr ""
"El color predeterminat del cursor. Aquest es mesclarà amb la imatge del "
"llenç de manera que generalment tindrà un color de contrast, però de vegades "
"aquesta mescla no funcionarà. Això sol ser causa de problemes en el "
"controlador. Quan passi això, podreu configurar aquí un color més agradable."

#: ../../reference_manual/preferences/general_settings.rst:115
msgid "Cursor Color:"
msgstr "Color del cursor:"

#: ../../reference_manual/preferences/general_settings.rst:120
msgid "Window Settings"
msgstr "Ajustaments per a la finestra"

#: ../../reference_manual/preferences/general_settings.rst:122
msgid "Multiple Document Mode"
msgstr "Mode múltiples documents"

#: ../../reference_manual/preferences/general_settings.rst:123
msgid ""
"This can be either tabbed like :program:`GIMP` or :program:`Painttool Sai`, "
"or subwindows, like :program:`Photoshop`."
msgstr ""
"Pot ser en pestanyes com el :program:`GIMP` o el :program:`Painttool Sai`, o "
"en subfinestres com el :program:`Photoshop`."

#: ../../reference_manual/preferences/general_settings.rst:124
msgid "Background image"
msgstr "Imatge de fons"

#: ../../reference_manual/preferences/general_settings.rst:125
msgid "Allows you to set a picture background for subwindow mode."
msgstr "Us permet establir una imatge de fons per al mode subfinestra."

#: ../../reference_manual/preferences/general_settings.rst:126
msgid "Window Background"
msgstr "Fons de la finestra"

#: ../../reference_manual/preferences/general_settings.rst:127
msgid "Set the color of the subwindow canvas area."
msgstr "Estableix el color de l'àrea del llenç de la subfinestra."

#: ../../reference_manual/preferences/general_settings.rst:128
msgid "Don't show contents when moving sub-windows"
msgstr "No mostra el contingut en moure les subfinestres"

#: ../../reference_manual/preferences/general_settings.rst:129
msgid ""
"This gives an outline when moving windows to work around ugly glitches with "
"certain graphics-cards."
msgstr ""
"Donarà un contorn en moure les finestres per evitar fallades lletges amb "
"certes targetes gràfiques."

#: ../../reference_manual/preferences/general_settings.rst:130
msgid "Show on-canvas popup messages"
msgstr "Mostra els missatges emergents en el llenç"

#: ../../reference_manual/preferences/general_settings.rst:131
msgid ""
"Whether or not you want to see the on-canvas pop-up messages that tell you "
"whether you are in tabbed mode, rotating the canvas, or mirroring it."
msgstr ""
"Si voleu o no veure els missatges emergents en el llenç que indiquen si "
"esteu en el mode en pestanyes, girant el llenç o emmirallant."

#: ../../reference_manual/preferences/general_settings.rst:132
msgid "Enable Hi-DPI support"
msgstr "Activa el funcionament amb Hi-DPI"

#: ../../reference_manual/preferences/general_settings.rst:133
msgid ""
"Attempt to use the Hi-DPI support. It is an option because we are still "
"experiencing bugs on windows."
msgstr ""
"Intentarà utilitzar el suport Hi-DPI. És una opció perquè encara estem "
"experimentant errors en el Windows."

#: ../../reference_manual/preferences/general_settings.rst:135
msgid "Allow only one instance of Krita"
msgstr "Permet només una única instància del Krita"

#: ../../reference_manual/preferences/general_settings.rst:135
msgid ""
"An instance is a single entry in your system's task manager. Turning this "
"option makes sure that Krita will check if there's an instance of Krita open "
"already when you instruct it to open new documents, and then have your "
"documents opened in that single instance. There's some obscure uses to "
"allowing multiple instances, but if you can't think of any, just keep this "
"option on."
msgstr ""
"Una instància és una sola entrada al gestor de tasques del sistema. En "
"activar aquesta opció, assegurareu que el Krita verificarà si hi ha una "
"instància oberta del Krita quan li indiqueu que obri documents nous, i "
"després els obrirà en aquesta única instància. Hi ha alguns usos foscos per "
"a permetre múltiples instàncies, però si no podeu pensar en cap, simplement "
"mantingueu activada aquesta opció."

#: ../../reference_manual/preferences/general_settings.rst:140
msgid "Tools Settings"
msgstr "Ajustaments per a les eines"

#: ../../reference_manual/preferences/general_settings.rst:142
msgid "In docker (default)"
msgstr "A l'acoblador (predeterminat)"

#: ../../reference_manual/preferences/general_settings.rst:143
msgid "Gives you the tool options in a docker."
msgstr "Oferirà les Opcions de l'eina en un acoblador."

#: ../../reference_manual/preferences/general_settings.rst:145
msgid "In toolbar"
msgstr "A la barra d'eines"

#: ../../reference_manual/preferences/general_settings.rst:145
msgid ""
"Gives you the tool options in the toolbar, next to the brush settings. You "
"can open it with the :kbd:`\\\\` key."
msgstr ""
"Donarà les Opcions de l'eina a la barra d'eines, al costat dels ajustaments "
"del pinzell. Podreu obrir-la amb la tecla :kbd:`\\\\`."

#: ../../reference_manual/preferences/general_settings.rst:148
msgid "Brush Flow Mode"
msgstr "Mode de fluir del pinzell"

#: ../../reference_manual/preferences/general_settings.rst:148
msgid ""
"In Krita 4.2 the behavior of flow in combination with opacity was changed. "
"This allows you to turn it back to the 4.1 behavior. This will however be "
"removed in future versions."
msgstr ""
"En el Krita 4.2 es va canviar el comportament del flux en combinació amb "
"l'opacitat. Permetrà tornar al comportament de la versió 4.1. Tanmateix, "
"serà eliminada en futures versions."

#: ../../reference_manual/preferences/general_settings.rst:151
msgid "Switch Control/Alt Selection Modifiers"
msgstr "Canvia la selecció de modificadors Control/Alt"

# skip-rule: kct-button
#: ../../reference_manual/preferences/general_settings.rst:151
msgid ""
"This switches the function of the :kbd:`Ctrl` and :kbd:`Alt` keys when "
"modifying selections. Useful for those used to Gimp instead of Photoshop, or "
"Lefties without a right :kbd:`Alt` key on their keyboard."
msgstr ""
"Canviarà la funció de les tecles :kbd:`Ctrl` i :kbd:`Alt` quan es modifiquen "
"les seleccions. Útil per als que estan acostumats al Gimp en lloc del "
"Photoshop o Lefties sense una tecla :kbd:`Alt` dreta en el seu teclat."

#: ../../reference_manual/preferences/general_settings.rst:154
msgid "Enable Touchpainting"
msgstr "Activa la pintura tàctil"

#: ../../reference_manual/preferences/general_settings.rst:154
msgid ""
"This allows finger painting with capacitive screens. Some devices have both "
"capacitive touch and a stylus, and then this can interfere. In that case, "
"just toggle this."
msgstr ""
"Permet pintar amb els dits en pantalles capacitives. Alguns dispositius "
"tenen un toc capacitiu i un llapiz, i això pot interferir. En aquest caso, "
"simplement canvieu això."

#: ../../reference_manual/preferences/general_settings.rst:156
msgid "Activate transform tool after pasting"
msgstr "Activa l'eina de transformació després d'enganxar"

#: ../../reference_manual/preferences/general_settings.rst:160
msgid ""
"A convenience feature. When enabling this, the transform tool will activate "
"after pasting for quick moving or rotating."
msgstr ""
"Una característica de comoditat. En habilitar-la, l'eina de transformació "
"s'activarà després d'enganxar per moure o girar amb rapidesa."

#: ../../reference_manual/preferences/general_settings.rst:163
msgid "This enables kinetic scrolling for scrollable areas."
msgstr "Permetrà el desplaçament cinètic per a les àrees desplaçables."

#: ../../reference_manual/preferences/general_settings.rst:168
msgid ""
"Kinetic scrolling on the brush chooser drop-down with activation mode set "
"to :guilabel:`On Click Drag`, with this disabled all of these clicks would "
"lead to a brush being selected regardless of drag motion."
msgstr ""
"El desplaçament cinètic a la llista desplegable del selector de pinzells amb "
"el mode d'activació establert a :guilabel:`Arrossega en fer clic`, amb "
"aquesta opció inhabilitada, tots aquests clics conduirien a la selecció d'un "
"pinzell independentment del moviment d'arrossegament."

#: ../../reference_manual/preferences/general_settings.rst:171
msgid "How it is activated."
msgstr "Com s'activarà."

#: ../../reference_manual/preferences/general_settings.rst:173
msgid "On Middle-Click Drag"
msgstr "Arrossega amb el clic del mig"

#: ../../reference_manual/preferences/general_settings.rst:174
msgid "Will activate when using the middle mouse button."
msgstr "S'activarà quan feu servir el botó central del ratolí."

#: ../../reference_manual/preferences/general_settings.rst:175
msgid "On Touch Drag"
msgstr "Arrossega en tocar"

#: ../../reference_manual/preferences/general_settings.rst:176
msgid "Will activate if it can recognize a touch event. May not always work."
msgstr ""
"S'activarà si es pot reconèixer un esdeveniment tàctil. Pot no funcionar "
"sempre."

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "Activation"
msgstr "Activació"

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "On Click Drag"
msgstr "Arrossega en fer clic"

#: ../../reference_manual/preferences/general_settings.rst:178
msgid "Will activate when it can recognize a click event, will always work."
msgstr ""
"S'activarà si es pot reconèixer un esdeveniment de clic, funcionarà sempre."

#: ../../reference_manual/preferences/general_settings.rst:180
msgid "Sensitivity"
msgstr "Sensibilitat"

#: ../../reference_manual/preferences/general_settings.rst:181
msgid ""
"How quickly the feature activates, this effective determines the length of "
"the drag."
msgstr ""
"La rapidesa amb la que s'activarà la característica, determinarà la durada "
"de l'arrossegament."

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Kinetic Scrolling (Needs Restart)"
msgstr "Desplaçament cinètic (cal reiniciar)"

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Hide Scrollbar"
msgstr "Oculta la barra de desplaçament"

#: ../../reference_manual/preferences/general_settings.rst:183
msgid "Whether to show scrollbars when doing this."
msgstr "Si es mostraran les barres de desplaçament."

#: ../../reference_manual/preferences/general_settings.rst:188
msgid "File Handling"
msgstr "Gestió de fitxers"

#: ../../reference_manual/preferences/general_settings.rst:192
msgid "Enable Autosaving"
msgstr "Activa el desat automàtic"

#: ../../reference_manual/preferences/general_settings.rst:193
msgid "Determines whether or not Krita should periodically autosave."
msgstr ""
"Determinarà si el Krita haurà o no de fer un desament automàtic "
"periòdicament."

#: ../../reference_manual/preferences/general_settings.rst:194
msgid "Autosave Every"
msgstr "Desa automàticament cada"

#: ../../reference_manual/preferences/general_settings.rst:195
msgid ""
"Here the user can specify how often Krita should autosave the file, you can "
"tick the checkbox to turn it off. For Windows these files are saved in the "
"%TEMP% directory. If you are on Linux it is stored in /home/'username'."
msgstr ""
"Aquí l'usuari podrà especificar amb quina freqüència el Krita haurà de desar "
"automàticament el fitxer, podreu demarcar la casella de selecció per a "
"desactivar-la. Per al Windows, aquests fitxers s'emmagatzemaran en el "
"directori %TEMP%. Si esteu al Linux, s'emmagatzemaran a /home/«nom_d'usuari»."

#: ../../reference_manual/preferences/general_settings.rst:196
msgid "Unnamed autosave files are hidden by default"
msgstr ""
"Els fitxers de desat automàtic sense nom són ocults de manera predeterminada"

#: ../../reference_manual/preferences/general_settings.rst:197
msgid ""
"This determines whether the filename of autosaves has a period prepended to "
"the name. On Linux and Mac OS this is a technique to ensure the file is "
"hidden by default."
msgstr ""
"Determinarà si el nom del fitxer del desament automàtic tindrà un punt "
"precedint al nom. Al Linux i Mac OS, aquesta és una tècnica predeterminada "
"per a garantir que el fitxer resti ocult."

#: ../../reference_manual/preferences/general_settings.rst:198
msgid "Create Backup File"
msgstr "Crea un fitxer de còpia de seguretat"

#: ../../reference_manual/preferences/general_settings.rst:199
msgid ""
"When selected Krita will, upon save, rename the original file as a backup "
"file and save the current image to the original name. The result is that you "
"will have saved the image, and there will be a copy of the image that is "
"saved separately as a backup. This is useful in case of crashes during saves."
msgstr ""
"Quan se selecciona, en desar, el Krita canviarà el nom del fitxer original "
"com un fitxer de còpia de seguretat i desarà la imatge actual amb el nom "
"original. El resultat serà que hi haureu desat la imatge i tindreu una còpia "
"de la imatge que es desarà per separat com a còpia de seguretat. És útil en "
"el cas de fallades durant els desaments."

#: ../../reference_manual/preferences/general_settings.rst:201
msgid "The default location these backups should be stored."
msgstr ""
"La ubicació predeterminada on s'hauran d'emmagatzemar aquestes còpies de "
"seguretat."

#: ../../reference_manual/preferences/general_settings.rst:203
msgid "Same Folder as Original File"
msgstr "Mateixa carpeta que el fitxer original"

#: ../../reference_manual/preferences/general_settings.rst:204
msgid "Store the file in the same folder as the original file was stored."
msgstr "Emmagatzemarà el fitxer a la mateixa carpeta que el fitxer original."

#: ../../reference_manual/preferences/general_settings.rst:205
msgid "User Folder"
msgstr "Carpeta d'usuari"

#: ../../reference_manual/preferences/general_settings.rst:206
msgid ""
"This is the main folder of your computer. On Linux and Mac OS this is the "
"'Home' folder, on Windows, the 'c:\\Users\\YOUR_USER_NAME' folder (where "
"YOUR_USER_NAME is your windows username)."
msgstr ""
"Aquesta és la carpeta principal del vostre ordinador. Al Linux i Mac OS "
"aquesta és la carpeta «Inici», al Windows, la carpeta «'c:\\Users"
"\\EL_VOSTRE_NOM_USUARI» (a on EL_VOSTRE_NOM_USUARI serà el vostre nom "
"d'usuari)."

#: ../../reference_manual/preferences/general_settings.rst:208
msgid "Backup File Location"
msgstr "Ubicació del fitxer de còpia de seguretat"

#: ../../reference_manual/preferences/general_settings.rst:208
msgid "Temporary File Folder"
msgstr "Carpeta del fitxer temporal"

#: ../../reference_manual/preferences/general_settings.rst:208
msgid ""
"This stored the file in the temp folder. Temp folders are special folders of "
"which the contents are emptied when you shut down your computer. If you "
"don't particularly care about your backup files and want them to be "
"'cleaned' automatically, this is the best place. If you want your backup "
"files to be kept indefinitely, this is a wrong choice."
msgstr ""
"Emmagatzemarà el fitxer a la carpeta temporal. Les carpetes temporals són "
"carpetes especials el contingut del qual es buidarà en apagar l'ordinador. "
"Si no us preocupeu especialment pels vostres fitxers de còpia de seguretat i "
"voleu que es «netegin» automàticament, aquest és el millor lloc. Si voleu "
"que els fitxers de còpia de seguretat es mantinguin indefinidament, aquesta "
"és una elecció incorrecta."

#: ../../reference_manual/preferences/general_settings.rst:210
msgid "Backup File Suffix"
msgstr "Sufix del fitxer de còpia de seguretat"

#: ../../reference_manual/preferences/general_settings.rst:211
msgid ""
"The suffix that will be placed after the full filename. 'filename.kra' will "
"then be saved as 'filename.kra~', ensuring the files won't show up in "
"Krita's open file dialog."
msgstr ""
"El sufix que es col·locarà després del nom complet del fitxer. El fitxer "
"«nom_fitxer.kra» es desarà com a «nom_fitxer.kra~», assegurant que els "
"fitxers no apareixeran al diàleg d'obrir fitxers del Krita."

#: ../../reference_manual/preferences/general_settings.rst:212
msgid "Number of Backup Files Kept"
msgstr "Nombre de fitxers de còpia de seguretat a mantenir"

#: ../../reference_manual/preferences/general_settings.rst:213
msgid ""
"Number of backup files Krita keeps, by default this is only one, but this "
"can be up to 99. Krita will then number the backup files."
msgstr ""
"El nombre de fitxers de còpia de seguretat que mantindrà el Krita, de manera "
"predeterminada només és un, però poden ser fins a 99. El Krita després "
"enumerarà els fitxers de còpia de seguretat."

# skip-rule: punctuation-period
#: ../../reference_manual/preferences/general_settings.rst:214
msgid "Compress \\*.kra files more."
msgstr "Comprimeix més els fitxers «\\*.kra»"

#: ../../reference_manual/preferences/general_settings.rst:215
msgid ""
"This increases the zip compression on the saved Krita files, which makes "
"them lighter on disk, but this takes longer to load."
msgstr ""
"Augmentarà la compressió ZIP en els fitxers desats pel Krita, el qual els "
"farà més lleugers en el disc, però es demoraran més en carregar."

#: ../../reference_manual/preferences/general_settings.rst:217
msgid "Kra files are zip files. Zip64 allows you to use"
msgstr "Els fitxers KRA són fitxers ZIP. El Zip64 us permetrà utilitzar-los."

#: ../../reference_manual/preferences/general_settings.rst:218
msgid "Use Zip64"
msgstr "Usa el Zip64"

#: ../../reference_manual/preferences/general_settings.rst:223
msgid "Miscellaneous"
msgstr "Miscel·lània"

#: ../../reference_manual/preferences/general_settings.rst:226
msgid ""
"This is the option for handling user sessions. It has the following options:"
msgstr ""
"Aquesta és l'opció per a gestionar les sessions d'usuari. Disposa de les "
"següents opcions:"

#: ../../reference_manual/preferences/general_settings.rst:228
msgid "Open Default Window"
msgstr "Obre la finestra predeterminada"

#: ../../reference_manual/preferences/general_settings.rst:229
msgid "This opens the regular empty window with the last used workspace."
msgstr ""
"Obrirà la finestra buida regular amb l'últim espai de treball utilitzat."

#: ../../reference_manual/preferences/general_settings.rst:230
msgid "Load Previous Session"
msgstr "Carrega la sessió anterior"

#: ../../reference_manual/preferences/general_settings.rst:231
msgid ""
"Load the last opened session. If you have :guilabel:`Save session when Krita "
"closes` toggled, this becomes the last files you had open and the like."
msgstr ""
"Carregarà la darrera sessió oberta. Si heu canviat :guilabel:`Desa la sessió "
"quan es tanqui el Krita`, esdevindrà en els últims fitxers que teníeu oberts "
"i similars."

#: ../../reference_manual/preferences/general_settings.rst:233
msgid "Show Session Manager"
msgstr "Mostra el gestor de sessions"

#: ../../reference_manual/preferences/general_settings.rst:233
msgid "Show the session manager directly so you can pick a session."
msgstr ""
"Mostrarà directament el gestor de sessions perquè pugueu triar una sessió."

#: ../../reference_manual/preferences/general_settings.rst:234
msgid "When Krita starts"
msgstr "Quan s'iniciï el Krita"

#: ../../reference_manual/preferences/general_settings.rst:237
msgid ""
"Save the current open windows, documents and the like into the current "
"session when closing Krita so you can resume where you left off."
msgstr ""
"Desarà les finestres obertes, els documents i similars a la sessió actual "
"quan tanqueu el Krita, de manera que pugueu reprendre des del lloc on ho heu "
"deixat."

#: ../../reference_manual/preferences/general_settings.rst:238
msgid "Save session when Krita closes"
msgstr "Desa la sessió quan es tanqui el Krita"

# skip-rule: punctuation-period
#: ../../reference_manual/preferences/general_settings.rst:240
msgid "Upon importing Images as Layers, convert to the image color space."
msgstr ""
"En importar les imatges com a capes, converteix a l'espai de color de la "
"imatge"

#: ../../reference_manual/preferences/general_settings.rst:241
msgid ""
"This makes sure that layers are the same color space as the image, necessary "
"for saving to PSD."
msgstr ""
"S'assegurarà que les capes tinguin el mateix espai de color que la imatge, "
"necessària per a desar a PSD."

#: ../../reference_manual/preferences/general_settings.rst:242
msgid "Undo Stack Size"
msgstr "Desfés la mida de la pila"

#: ../../reference_manual/preferences/general_settings.rst:243
msgid ""
"This is the number of undo commands Krita remembers. You can set the value "
"to 0 for unlimited undos."
msgstr ""
"Aquest és el nombre d'ordres a desfer que recordarà el Krita. Podreu "
"establir el valor a 0 per a que sigui il·limitat."

#: ../../reference_manual/preferences/general_settings.rst:244
msgid "Favorite Presets"
msgstr "Predefinits preferits"

#: ../../reference_manual/preferences/general_settings.rst:245
msgid ""
"This determines the amount of presets that can be used in the pop-up palette."
msgstr ""
"Determinarà la quantitat de predefinits que es podran utilitzar a la paleta "
"emergent."

#: ../../reference_manual/preferences/general_settings.rst:247
msgid ""
"This'll hide the splash screen automatically once Krita is fully loaded."
msgstr ""
"Ocultarà automàticament la pantalla de presentació una vegada s'hagi "
"carregat completament el Krita."

# skip-rule: punctuation-period
#: ../../reference_manual/preferences/general_settings.rst:251
msgid "Hide splash screen on startup."
msgstr "No mostris la pantalla de presentació"

#: ../../reference_manual/preferences/general_settings.rst:251
msgid ""
"Deprecated because Krita now has a welcome widget when no canvas is open."
msgstr ""
"Obsoleta perquè el Krita ara té un estri de benvinguda quan no hi ha cap "
"llenç obert."

#: ../../reference_manual/preferences/general_settings.rst:253
msgid "Enable Native File Dialog"
msgstr "Activa el diàleg de fitxers nadiu"

#: ../../reference_manual/preferences/general_settings.rst:254
msgid ""
"This allows you to use the system file dialog. By default turned off because "
"we cannot seem to get native file dialogues 100% bugfree."
msgstr ""
"Permet utilitzar el diàleg de fitxers del sistema. De manera predeterminada "
"està desactivada perquè sembla que no podem obtenir diàlegs de fitxers "
"nadius 100% lliures d'errors."

#: ../../reference_manual/preferences/general_settings.rst:255
msgid "Maximum brush size"
msgstr "Mida màxima del pinzell"

#: ../../reference_manual/preferences/general_settings.rst:256
msgid ""
"This allows you to set the maximum brush size to a size of up to 10.000 "
"pixels. Do be careful with using this, as a 10.000 pixel size can very "
"quickly be a full gigabyte of data being manipulated, per dab. In other "
"words, this might be slow."
msgstr ""
"Permet establir la mida màxima del pinzell a una mida de fins a 10.000 "
"píxels. Tingueu compte amb això, ja que una mida de 10.000 píxels pot "
"esdevindre molt ràpidament en tot un gigabyte de dades que es manipulen, per "
"toc. En altres paraules, això podria ser lent."

#: ../../reference_manual/preferences/general_settings.rst:258
msgid "Krita will recalculate the cache when you're not doing anything."
msgstr "El Krita recalcularà la memòria cau quan no estigueu fent res."

#: ../../reference_manual/preferences/general_settings.rst:262
msgid ""
"This is now in the :ref:`performance_settings` under :guilabel:`Animation "
"Cache`."
msgstr ""
"Ara es troba als :ref:`performance_settings` sota la :guilabel:`Memòria cau "
"de l'animació`."
