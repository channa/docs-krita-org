# Translation of docs_krita_org_reference_manual___tools___reference_images_tool.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-24 16:04+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: clic esquerre del ratolí"

#: ../../<rst_epilog>:84
msgid ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"
msgstr ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: eina d'imatges de referència"

#: ../../reference_manual/tools/reference_images_tool.rst:1
msgid "The reference images tool"
msgstr "Referència de l'eina Imatges de referència del Krita."

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Reference"
msgstr "Referència"

#: ../../reference_manual/tools/reference_images_tool.rst:15
msgid "Reference Images Tool"
msgstr "Eina d'imatges de referència"

#: ../../reference_manual/tools/reference_images_tool.rst:17
msgid "|toolreference|"
msgstr "|toolreference|"

#: ../../reference_manual/tools/reference_images_tool.rst:21
msgid ""
"The reference images tool is a replacement for the reference images docker. "
"You can use it to load images from your disk as reference, which can then be "
"moved around freely on the canvas and placed wherever."
msgstr ""
"L'eina d'imatges de referència és una substitució per a l'acoblador Imatges "
"de referència. Podeu utilitzar-la per a carregar imatges del disc com a "
"referència, les quals després es podran moure lliurement sobre el llenç i "
"situar-les on calgui."

#: ../../reference_manual/tools/reference_images_tool.rst:24
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/reference_images_tool.rst:26
msgid "Add reference image"
msgstr "Afegeix una imatge de referència"

#: ../../reference_manual/tools/reference_images_tool.rst:27
msgid "Load a single image to display on the canvas."
msgstr "Carrega una sola imatge per a mostrar-la al llenç."

#: ../../reference_manual/tools/reference_images_tool.rst:28
msgid "Load Set"
msgstr "Carrega un conjunt"

#: ../../reference_manual/tools/reference_images_tool.rst:29
msgid "Load a set of reference images."
msgstr "Carrega un conjunt d'imatges de referència."

#: ../../reference_manual/tools/reference_images_tool.rst:30
msgid "Save Set"
msgstr "Desa el conjunt"

#: ../../reference_manual/tools/reference_images_tool.rst:31
msgid "Save a set of reference images."
msgstr "Desa un conjunt d'imatges de referència."

#: ../../reference_manual/tools/reference_images_tool.rst:32
msgid "Delete all reference images"
msgstr "Suprimeix totes les imatges de referència"

#: ../../reference_manual/tools/reference_images_tool.rst:33
msgid "Delete all the reference images"
msgstr "Suprimeix totes les imatges de referència."

#: ../../reference_manual/tools/reference_images_tool.rst:34
msgid "Keep aspect ratio"
msgstr "Mantén la relació d'aspecte"

#: ../../reference_manual/tools/reference_images_tool.rst:35
msgid "When toggled this will force the image to not get distorted."
msgstr "Quan s'alterna, obligarà a la imatge a no distorsionar-se."

#: ../../reference_manual/tools/reference_images_tool.rst:36
msgid "Opacity"
msgstr "Opacitat"

#: ../../reference_manual/tools/reference_images_tool.rst:37
msgid "Lower the opacity."
msgstr "Baixa l'opacitat."

#: ../../reference_manual/tools/reference_images_tool.rst:38
msgid "Saturation"
msgstr "Saturació"

#: ../../reference_manual/tools/reference_images_tool.rst:39
msgid ""
"Desaturate the image. This is useful if you only want to focus on the light/"
"shadow instead of getting distracted by the colors."
msgstr ""
"Dessatura la imatge. Això és útil si només voleu enfocar la llum/ombra en "
"lloc de distreure amb els colors."

#: ../../reference_manual/tools/reference_images_tool.rst:41
msgid "How is the reference image stored."
msgstr "Com emmagatzemar la imatge de referència."

#: ../../reference_manual/tools/reference_images_tool.rst:43
msgid "Embed to \\*.kra"
msgstr "Incrusta al \\*.kra"

#: ../../reference_manual/tools/reference_images_tool.rst:44
msgid ""
"Store this reference image into the kra file. This is recommended for small "
"vital files you'd easily lose track of otherwise."
msgstr ""
"Emmagatzema aquesta imatge de referència al fitxer kra. Això és el recomanat "
"per als fitxers vitals petits que d'altra manera es perdrien amb facilitat."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Storage mode"
msgstr "Mode d'emmagatzematge"

# skip-rule: punctuation-period
#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Link to external file."
msgstr "Enllaça a un fitxer extern"

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid ""
"Only link to the reference image, krita will open it from the disk everytime "
"it loads this file. This is recommended for big files, or files that change "
"a lot."
msgstr ""
"Només enllaça amb la imatge de referència, el Krita l'obrirà des del disc "
"cada vegada que carregui aquest fitxer. Això es recomana per a fitxers "
"grans, o fitxers que canvien molt."

#: ../../reference_manual/tools/reference_images_tool.rst:48
msgid ""
"You can move around reference images by selecting them with |mouseleft|, and "
"dragging them. You can rotate reference images by holding the cursor close "
"to the outside of the corners till the rotate cursor appears, while tilting "
"is done by holding the cursor close to the outside of the middle nodes. "
"Resizing can be done by dragging the nodes. You can delete a single "
"reference image by clicking it and pressing the :kbd:`Del` key. You can "
"select multiple reference images with the :kbd:`Shift` key and perform all "
"of these actions."
msgstr ""
"Podeu anar per les imatges de referència seleccionant-les fent |mouseleft| i "
"arrossegant. Les podeu girar mantenint el cursor a prop de l'exterior de les "
"cantonades fins que aparegui el cursor de gir, mentre que la inclinació es "
"realitza mantenint el cursor prop de l'exterior dels nodes centrals. El "
"canvi de mida es pot fer arrossegant els nodes. Podeu suprimir una sola "
"imatge de referència fent clic sobre seu i prement la tecla :kbd:`Supr`. "
"Podeu seleccionar múltiples imatges de referència amb la tecla :kbd:`Majús.` "
"i realitzant totes aquestes accions."

#: ../../reference_manual/tools/reference_images_tool.rst:50
msgid ""
"To hide all reference images temporarily use :menuselection:`View --> Show "
"Reference Images`."
msgstr ""
"Per ocultar totes les imatges de referència, utilitzeu :menuselection:"
"`Visualitza --> Mostra les images de referència`."
