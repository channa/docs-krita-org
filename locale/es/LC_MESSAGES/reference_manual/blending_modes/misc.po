# Spanish translations for docs_krita_org_reference_manual___blending_modes___misc.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___blending_modes___misc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-06-19 21:55+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr "Mapa normal"

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "Copiar"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "Izquierda: **Normal**. Derecha: **Copiar**."

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr "Copiar rojo"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr "Copiar verde"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr "Copiar azul"

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "Copiar rojo, verde, azul"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "Izquierda: **Normal**. Derecha: **Copiar rojo**."

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "Izquierda: **Normal**. Derecha: **Copiar verde**."

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "Izquierda: **Normal**. Derecha: **Copiar azul**."

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "Disolver"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "Izquierda: **Normal**. Derecha: **Disolver**."
