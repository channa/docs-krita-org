# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:29+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Autosave on split"
msgstr "Spara automatiskt vid delning"

#: ../../reference_manual/image_split.rst:1
msgid "The Image Split functionality in Krita"
msgstr "Funktionen för att dela bilder i Krita"

#: ../../reference_manual/image_split.rst:10
msgid "Splitting"
msgstr "Delning"

#: ../../reference_manual/image_split.rst:15
msgid "Image Split"
msgstr "Dela bild"

#: ../../reference_manual/image_split.rst:17
msgid ""
"Found under :menuselection:`Image --> Image Split`, the Image Split function "
"allows you to evenly split a document up into several sections. This is "
"useful for splitting up spritesheets for example."
msgstr ""
"Bilddelningsfunktionen hittar man under :menuselection:`Bild --> Dela bild`, "
"vilken låter dig jä,t dela upp ett dokument i olika sektioner. Det är "
"användbart för att exempelvis dela upp figurblad."

#: ../../reference_manual/image_split.rst:19
msgid "Horizontal Lines"
msgstr "Horisontella linjer"

#: ../../reference_manual/image_split.rst:20
msgid ""
"The amount of horizontal lines to split at. 4 lines will mean that the image "
"is split into 5 horizontal stripes."
msgstr ""
"Antal horisontella linjer att dela vid. Fyra linjer betyder att bilden delas "
"i fem horisontella band."

#: ../../reference_manual/image_split.rst:22
msgid "Vertical Lines"
msgstr "Vertikala linjer"

#: ../../reference_manual/image_split.rst:22
msgid ""
"The amount of vertical lines to split at. 4 lines will mean that the image "
"is split into 5 vertical stripes."
msgstr ""
"Antal vertikala linjer att dela vid. Fyra linjer betyder att bilden delas i "
"fem vertikala band."

#: ../../reference_manual/image_split.rst:24
msgid "Sort Direction"
msgstr "Sorteringsriktning:"

#: ../../reference_manual/image_split.rst:28
msgid "Whether to number the files using the following directions:"
msgstr "Om filerna ska numreras med användning av följande riktningar:"

#: ../../reference_manual/image_split.rst:30
msgid "Horizontal"
msgstr "Horisontell"

#: ../../reference_manual/image_split.rst:31
msgid "Left to right, top to bottom."
msgstr "Vänster till höger, uppåt till neråt"

#: ../../reference_manual/image_split.rst:33
msgid "Vertical"
msgstr "Vertikal"

#: ../../reference_manual/image_split.rst:33
msgid "Top to bottom, left to right."
msgstr "Uppåt till neråt, vänster till höger"

#: ../../reference_manual/image_split.rst:35
msgid "Prefix"
msgstr "Prefix"

#: ../../reference_manual/image_split.rst:36
msgid ""
"The prefix at which the files should be saved at. By default this is the "
"current document name."
msgstr ""
"Prefix som filerna ska sparas med. Normalt är det aktuellt dokumentnamn."

#: ../../reference_manual/image_split.rst:37
msgid "File Type"
msgstr "Filtyp"

#: ../../reference_manual/image_split.rst:38
msgid "Which file format to save to."
msgstr "Vilket filformat som ska sparas med."

#: ../../reference_manual/image_split.rst:40
msgid ""
"This will result in all slices being saved automatically using the above "
"prefix. Otherwise Krita will ask the name for each slice."
msgstr ""
"Det resulterar i att alla skivor sparas automatiskt med ovanstående prefix. "
"Annars frågar Krita efter namn på varje skiva."
