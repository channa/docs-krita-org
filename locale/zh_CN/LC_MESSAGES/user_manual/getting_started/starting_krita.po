msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___getting_started___starting_krita.pot\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: 手绘笔刷工具"

#: ../../user_manual/getting_started/starting_krita.rst:None
msgid ".. image:: images/Starting-krita.png"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:1
msgid ""
"A simple guide to the first basic steps of using Krita: creating and saving "
"an image."
msgstr "使用 Krita 的第一步：创建和保存图像。"

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Getting started"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Save"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "Load"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:18
msgid "New"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:22
msgid "Starting Krita"
msgstr "新建和保存文档"

#: ../../user_manual/getting_started/starting_krita.rst:24
msgid ""
"When you start Krita for the first time there will be no canvas or new "
"document open by default. You will be greeted by a :ref:`welcome screen "
"<welcome_screen>`, which will have option to create a new file or open "
"existing document. To create a new canvas you have to create a new document "
"from the :guilabel:`File` menu or by clicking on :guilabel:`New File`  under "
"start section of the welcome screen. This will open the new file dialog box. "
"If you want to open an existing image, either use :menuselection:`File --> "
"Open` or drag the image from your computer into Krita's window."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:37
msgid "Creating a New Document"
msgstr "新建文档"

#: ../../user_manual/getting_started/starting_krita.rst:39
msgid "A new document can be created as follows."
msgstr "新文档的创建方式如下。"

#: ../../user_manual/getting_started/starting_krita.rst:41
msgid "Click on :guilabel:`File` from the application menu at the top."
msgstr "在应用程序的顶部菜单点击 :guilabel:`文件`。"

#: ../../user_manual/getting_started/starting_krita.rst:42
msgid ""
"Then click on :guilabel:`New`. Or you can do this by pressing the :kbd:`Ctrl "
"+ N` shortcut."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:43
msgid "Now you will get a New Document dialog box as shown below:"
msgstr "Krita 会显示一个新建文档对话框："

#: ../../user_manual/getting_started/starting_krita.rst:46
msgid ".. image:: images/Krita_newfile.png"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:47
msgid ""
"Click on the :guilabel:`Custom Document` section and in the :guilabel:"
"`Dimensions` tab choose A4 (300ppi) or any size that you prefer from the :"
"guilabel:`predefined` drop down To know more about the other sections such "
"as create document from clipboard and templates see :ref:"
"`create_new_document`."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:52
msgid ""
"Make sure that the color profile is RGB and depth is set to 8-bit integer/"
"channel in the color section. For advanced information about the color and "
"color management refer to :ref:`general_concept_color`."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:56
msgid "How to use brushes"
msgstr "如何使用笔刷"

#: ../../user_manual/getting_started/starting_krita.rst:58
msgid ""
"Now, on the blank white canvas, just left click with your mouse or draw with "
"the pen on a graphic tablet. If everything's correct, you should be able to "
"draw on the canvas! The brush tool should be selected by default when you "
"start Krita, but if for some reason it is not, you can click on this |"
"toolfreehandbrush| icon from the toolbox and activate the brush tool."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:64
msgid ""
"Of course, you'd want to use different brushes. On your right, there's a "
"docker named Brush Presets (or on top, press the :kbd:`F6` key to find this "
"one) with all these cute squares with pens and crayons."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:68
msgid ""
"If you want to tweak the presets, check the Brush Editor in the toolbar. You "
"can also access the Brush Editor with the :kbd:`F5` key."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:72
msgid ".. image:: images/dockers/Krita_Brush_Preset_Docker.png"
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:73
msgid ""
"Tick any of the squares to choose a brush, and then draw on the canvas. To "
"change color, click the triangle in the Advanced Color Selector docker."
msgstr ""
"点击任意一个方块就等于选中了该笔刷，它会被用于在画布上作画。要更改颜色，可以"
"在高级拾色器面板的三角形色块里点击选择。"

#: ../../user_manual/getting_started/starting_krita.rst:77
msgid "Erasing"
msgstr "擦除"

#: ../../user_manual/getting_started/starting_krita.rst:79
msgid ""
"There are brush presets for erasing, but it is often faster to use the "
"eraser toggle. By toggling the :kbd:`E` key, your current brush switches "
"between erasing and painting. This erasing method works with most of the "
"tools. You can erase using the line tool, rectangle tool, and even the "
"gradient tool."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:85
msgid "Saving and opening files"
msgstr "保存文件"

#: ../../user_manual/getting_started/starting_krita.rst:87
msgid ""
"Now, once you have figured out how to draw something in Krita, you may want "
"to save it. The save option is in the same place as it is in all other "
"computer programs: the top-menu of :guilabel:`File`, and then :guilabel:"
"`Save`. Select the folder you want to have your drawing, and select the file "
"format you want to use ('.kra' is Krita's default format, and will save "
"everything). And then hit :guilabel:`Save`. Some older versions of Krita "
"have a bug and require you to manually type the extension."
msgstr ""

#: ../../user_manual/getting_started/starting_krita.rst:95
msgid ""
"If you want to show off your image on the internet, check out the :ref:"
"`saving_for_the_web` tutorial."
msgstr ""
"如果你想要在互联网上分享你的图像，请查阅 :ref:`saving_for_the_web` 教程。"

#: ../../user_manual/getting_started/starting_krita.rst:98
msgid ""
"Check out :ref:`navigation` for further basic information, :ref:"
"`basic_concepts` for an introduction as Krita as a medium, or just go out "
"and explore Krita!"
msgstr ""
"你还可在 :ref:`navigation` 一节学习更多基础知识，在 :ref:`basic_concepts` 一"
"节学习在 Krita 里进行数字绘画的要点。现在开始大胆地探索使用 Krita 吧！"
