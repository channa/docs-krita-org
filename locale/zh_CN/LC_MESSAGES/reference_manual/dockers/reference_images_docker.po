msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___reference_images_docker.pot\n"

#: ../../reference_manual/dockers/reference_images_docker.rst:1
msgid "Overview of the pattern docker."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:12
msgid "Reference"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:17
msgid "Reference Images Docker"
msgstr "参考图像"

#: ../../reference_manual/dockers/reference_images_docker.rst:21
msgid ""
"This docker was removed in Krita 4.0 due to crashes on Windows. :ref:`The "
"reference images tool in 4.1 replaces it. <reference_images_tool>`"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:24
msgid ""
".. image:: images/dockers/400px-Krita_Reference_Images_Browse_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:26
msgid ".. image:: images/dockers/400px-Krita_Reference_Images_Image_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:27
msgid ""
"This docker allows you to pick an image from outside of Krita and use it as "
"a reference. Even better, you can pick colors from it directly."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:29
msgid "The docker consists of two tabs: Browsing and Image."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:32
msgid "Browsing"
msgstr "浏览"

#: ../../reference_manual/dockers/reference_images_docker.rst:34
msgid ""
"Browsing gives you a small file browser, so you can navigate to the map "
"where the image you want to use as reference is located."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:36
msgid ""
"There's an image strip beneath the browser, allowing you to select the image "
"which you want to use. Double click to load it in the :guilabel:`Image` tab."
msgstr ""

#: ../../reference_manual/dockers/reference_images_docker.rst:39
msgid "Image"
msgstr "图像"

#: ../../reference_manual/dockers/reference_images_docker.rst:41
msgid ""
"This tab allows you to see the images you selected, and change the zoom "
"level. Clicking anywhere on the image will allow you to pick the merged "
"color from it. Using the cross symbol, you can remove the icon."
msgstr ""
