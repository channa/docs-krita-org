# translation of docs_krita_org_resources_page.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_resources_page\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-03-13 12:31+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-deevadBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-deevadBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-deevadBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-mirandaBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-mirandaBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-mirandaBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-conceptBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-conceptBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-conceptBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-aldyBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-aldyBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-aldyBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-vascoBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-vascoBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-vascoBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-meemodrawsBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-meemodrawsBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-meemodrawsBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-stalcryBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-stalcryBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-stalcryBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-woltheraBrushes.jpg"
msgid ".. image:: images/resource_packs/Resources-woltheraBrushes.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-woltheraBrushes.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-nylnook.jpg"
msgid ".. image:: images/resource_packs/Resources-nylnook.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-nylnook.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-hushcoilBrushes.png"
msgid ".. image:: images/resource_packs/Resources-hushcoilBrushes.png"
msgstr ".. image:: images/en/resource_packs/Resources-hushcoilBrushes.png"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-raghukamathBrushes.png"
msgid ".. image:: images/resource_packs/Resources-raghukamathBrushes.png"
msgstr ".. image:: images/en/resource_packs/Resources-raghukamathBrushes.png"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-GDQuestBrushes.jpeg"
msgid ".. image:: images/resource_packs/Resources-GDQuestBrushes.jpeg"
msgstr ".. image:: images/en/resource_packs/Resources-GDQuestBrushes.jpeg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-iForce73Brushes.png"
msgid ".. image:: images/resource_packs/Resources-iForce73Brushes.png"
msgstr ".. image:: images/en/resource_packs/Resources-iForce73Brushes.png"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-deevadTextures.jpg"
msgid ".. image:: images/resource_packs/Resources-deevadTextures.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-deevadTextures.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/Resources-deevadTextures2.jpg"
msgid ".. image:: images/resource_packs/Resources-deevadTextures2.jpg"
msgstr ".. image:: images/en/resource_packs/Resources-deevadTextures2.jpg"

#: ../../resources_page.rst:0
#, fuzzy
#| msgid ".. image:: images/en/resource_packs/simon_pixel_art_course.png"
msgid ".. image:: images/resource_packs/simon_pixel_art_course.png"
msgstr ".. image:: images/en/resource_packs/simon_pixel_art_course.png"

#: ../../resources_page.rst:1
msgid "Resource Packs for Krita."
msgstr "Balíky zdrojov pre Krita."

#: ../../resources_page.rst:18
msgid "Resources"
msgstr "Zdroje"

#: ../../resources_page.rst:21
msgid "Brush Packs"
msgstr "Balíky štetcov"

#: ../../resources_page.rst:28 ../../resources_page.rst:87
#: ../../resources_page.rst:91
msgid "David Revoy"
msgstr "David Revoy"

#: ../../resources_page.rst:32
msgid "Ramon Miranda"
msgstr "Ramon Miranda"

#: ../../resources_page.rst:36
msgid "Concept art & Illustration Pack"
msgstr ""

#: ../../resources_page.rst:40
msgid "Al-dy"
msgstr "Al-dy"

#: ../../resources_page.rst:44
msgid "Vasco Basqué"
msgstr "Vasco Basqué"

#: ../../resources_page.rst:48
msgid "Meemodraws"
msgstr ""

#: ../../resources_page.rst:52
msgid "Stalcry"
msgstr "Stalcry"

#: ../../resources_page.rst:56
msgid "Wolthera"
msgstr "Wolthera"

#: ../../resources_page.rst:60
msgid "Nylnook"
msgstr "Nylnook"

#: ../../resources_page.rst:64
msgid "Hushcoil"
msgstr ""

#: ../../resources_page.rst:68
msgid "Raghukamath"
msgstr ""

#: ../../resources_page.rst:72
msgid "GDQuest"
msgstr "GDQuest"

#: ../../resources_page.rst:80
msgid "Texture Packs"
msgstr "Balíky textúr"

#: ../../resources_page.rst:94
msgid "External tutorials"
msgstr ""

#: ../../resources_page.rst:101
msgid "Simón Sanchez' \"Learn to Create Pixel Art from Zero\" course on Udemy"
msgstr ""

#: ../../resources_page.rst:104
msgid "User-made Python Plugins"
msgstr ""

#: ../../resources_page.rst:105
msgid ""
"To install and manage your plugins, visit the :ref:"
"`krita_python_plugin_howto` area. See the second area on how to get Krita to "
"recognize your plugin."
msgstr ""

#: ../../resources_page.rst:107
msgid "Direct Eraser Plugin"
msgstr ""

#: ../../resources_page.rst:109
#, fuzzy
#| msgid ""
#| "http://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin."
#| "zip"
msgid ""
"https://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin.zip"
msgstr ""
"http://www.mediafire.com/file/sotzc2keogz0bor/Krita+Direct+Eraser+Plugin.zip"

#: ../../resources_page.rst:111
msgid "Tablet Controls Docker"
msgstr ""

#: ../../resources_page.rst:113
msgid "https://github.com/tokyogeometry/tabui"
msgstr "https://github.com/tokyogeometry/tabui"

#: ../../resources_page.rst:115
msgid "On-screen Canvas Shortcuts"
msgstr ""

#: ../../resources_page.rst:117
msgid ""
"https://github.com/qeshi/henriks-onscreen-krita-shortcut-buttons/tree/master/"
"henriks_krita_buttons"
msgstr ""
"https://github.com/qeshi/henriks-onscreen-krita-shortcut-buttons/tree/master/"
"henriks_krita_buttons"

#: ../../resources_page.rst:119
msgid "Spine File Format Export"
msgstr ""

#: ../../resources_page.rst:121
msgid "https://github.com/chartinger/krita-unofficial-spine-export"
msgstr "https://github.com/chartinger/krita-unofficial-spine-export"

#: ../../resources_page.rst:123
msgid "GDQuest - Designer Tools"
msgstr ""

#: ../../resources_page.rst:125
msgid "https://github.com/GDquest/Krita-designer-tools"
msgstr "https://github.com/GDquest/Krita-designer-tools"

#: ../../resources_page.rst:127
msgid "AnimLayers (Animate with Layers)"
msgstr ""

#: ../../resources_page.rst:129
msgid "https://github.com/thomaslynge/krita-plugins"
msgstr "https://github.com/thomaslynge/krita-plugins"

#: ../../resources_page.rst:131
msgid "Art Revision Control (using GIT)"
msgstr ""

#: ../../resources_page.rst:133
msgid "https://github.com/abeimler/krita-plugin-durra"
msgstr "https://github.com/abeimler/krita-plugin-durra"

#: ../../resources_page.rst:135
msgid "Krita Plugin generator"
msgstr ""

#: ../../resources_page.rst:137
msgid "https://github.com/cg-cnu/vscode-krita-plugin-generator"
msgstr "https://github.com/cg-cnu/vscode-krita-plugin-generator"

#: ../../resources_page.rst:139
msgid "Bash Action (works with OSX and Linux)"
msgstr ""

#: ../../resources_page.rst:141
msgid ""
"https://github.com/juancarlospaco/krita-plugin-bashactions#krita-plugin-"
"bashactions"
msgstr ""
"https://github.com/juancarlospaco/krita-plugin-bashactions#krita-plugin-"
"bashactions"

#: ../../resources_page.rst:143
msgid "Reference Image Docker (old style)"
msgstr ""

#: ../../resources_page.rst:145
msgid "https://github.com/antoine-roux/krita-plugin-reference"
msgstr "https://github.com/antoine-roux/krita-plugin-reference"

#: ../../resources_page.rst:147
msgid "Post images on Mastadon"
msgstr ""

#: ../../resources_page.rst:149
msgid "https://github.com/spaceottercode/kritatoot"
msgstr "https://github.com/spaceottercode/kritatoot"

#: ../../resources_page.rst:151
msgid "Python auto-complete for text editors"
msgstr ""

#: ../../resources_page.rst:153
msgid "https://github.com/scottpetrovic/krita-python-auto-complete"
msgstr "https://github.com/scottpetrovic/krita-python-auto-complete"

#: ../../resources_page.rst:157
msgid "See Something We Missed?"
msgstr ""

#: ../../resources_page.rst:158
msgid ""
"Have a resource you made and want to to share it with other artists? Let us "
"know in the forum or visit our chat room to discuss getting the resource "
"added to here."
msgstr ""
